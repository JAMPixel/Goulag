/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.EntityModule = function () {
    let exports = {};

    class Entity
    {
        /**
         * Constructor.
         * @param x Position X.
         * @param y Position Y.
         * @param speed Entity's speed.
         * @param sprite Entity's sprite.
         */
        constructor(x, y, speed, sprite)
        {
            this._x = x;
            this._y = y;

            this._speed = speed;

            this._sprite = sprite;
        }

        /**
         * Move entity.
         * @param offsetX Offset X.
         * @param offsetY Offset Y.
         */
        move(offsetX, offsetY)
        {
            this._x += offsetX * this._speed;
            this._y += offsetY * this._speed;
        }

        /**
         * @brief Render Entity.
         * @param renderManager the RenderManager to use for rendering.
         * @param offsetX Offset X to apply to draw.
         * @param offsetY Offset Y to apply to draw.
         */
        render(renderManager, offsetX = 0, offsetY = 0)
        {
            this._sprite.update();

            this._sprite.render(renderManager, this._x + offsetX, this._y + offsetY);
        }

        /**
         * Define the new sprite to use for Entity.
         * @param sprite Sprite.
         */
        setSprite(sprite)
        {
            this._sprite = sprite;
        }

        /**
         * @brief Render Entity
         * @param renderManager the RenderManager to use for rendering
         */
        renderFix(renderManager)
        {
            this._sprite.update();

            this._sprite.render(renderManager, 0, 0);
        }
    }

    exports.EntityClass = Entity;

    return exports;
};