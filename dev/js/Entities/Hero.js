/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.HeroModule = function () {
    let exports = {};

    const ResourceManager = getModules.ResourceManagerInstance;
    const Random = getModules.RandomModule().RandomClass;

    class Hero extends getModules.EntityModule().EntityClass
    {
        /**
         * Constructor.
         * @param x Position X.
         * @param y Position Y.
         * @param speed Entity's speed.
         * @param sprite Entity's sprite.
         */
        constructor(x, y, speed, sprite)
        {
            super(x, y, speed, sprite);

            this._isAlive = true;

            this._callbackSprite = undefined;
        }

        /**
         * Cancel the current timeout if there is one.
         */
        cancelTimeout()
        {
            // Cancel previous timeout if there is one
            if (this._callbackSprite != undefined)
            {
                clearTimeout(this._callbackSprite);
            }
        }

        /**
         * Define the new sprite to use for Entity.
         * @param sprite Sprite.
         */
        setSprite(sprite)
        {
            // Cancel previous timeout if there is one
            this.cancelTimeout();

            super.setSprite(sprite);

            // Add timeout to put entity in stand by mode after a certain amount of time
            this._callbackSprite = setTimeout(function (object) {
                // Random with bias
                if (Random.getRandom(0, 100) > 35)
                {
                    object._sprite = ResourceManager.getSprite("hero-standby2")
                }
                else
                {
                    object._sprite = ResourceManager.getSprite("hero-standby1")
                }
            }, 600, this);
        }

        /**
         * Make hero dieing.
         */
        die()
        {
            this.cancelTimeout();

            if (this._isAlive)
            {
                super.setSprite(ResourceManager.getSprite("hero-death"));
            }

            this._isAlive = false;
        }
    }

    exports.HeroClass = Hero;

    return exports;
};