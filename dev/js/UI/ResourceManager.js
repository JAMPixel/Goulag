/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.ResourceManagerInstance = undefined;

getModules.ResourceManagerModule = function () {
    let exports = {};

    const Sprite = getModules.SpriteModule().SpriteClass;
    const Texture = getModules.TextureModule().TextureClass;

    class ResourceManager
    {
        constructor()
        {
            this._textures = new Map();
            this._sprites = new Map();
        }

        setTexture(key, value)
        {
            this._textures.set(key, value);
        }

        getTexture(key)
        {
            return this._textures.get(key);
        }

        setSprite(key, value)
        {
            this._sprites.set(key, value);
        }

        getSprite(key)
        {
            return this._sprites.get(key);
        }

        loadSprite(spriteId, spriteKey, size, nbFrames, nbTicksPerFrame, loop = true)
        {
            let spriteImage = document.querySelector(spriteId);
            let sprite = new Sprite({
                sizeX: size,
                sizeY: size,
                image: spriteImage,
                numberOfFrames: nbFrames,
                ticksPerFrame: nbTicksPerFrame,    // For a 60 fps refresh display sprite at 15 fps
                loop: loop
            });

            this.setSprite(spriteKey, sprite);
        }

        loadTexture(textureId, textureKey, size)
        {
            let textureImage = document.querySelector(textureId);
            let texture = new Texture({
                sizeX: size,
                sizeY: size,
                image: textureImage
            });

            this.setTexture(textureKey, texture);
        }

        loadResources()
        {
            // Load sprites
            this.loadSprite("#hero-move-top-sprite-image", "hero-top", 0.75, 4, 4);
            this.loadSprite("#hero-move-bottom-sprite-image", "hero-bottom", 0.75, 4, 4);
            this.loadSprite("#hero-move-left-sprite-image", "hero-left", 0.75, 4, 4);
            this.loadSprite("#hero-move-right-sprite-image", "hero-right", 0.75, 4, 4);
            this.loadSprite("#hero-standby1-sprite-image", "hero-standby1", 0.75, 7, 4);
            this.loadSprite("#hero-standby2-sprite-image", "hero-standby2", 0.75, 5, 4);
            this.loadSprite("#hero-death-sprite-image", "hero-death", 0.75, 4, 15, false);

            this.loadSprite("#enemy-ghost-sprite-image", "ghost", 0.75, 4, 15);

            // Load textures
            this.loadTexture("#wall-image", "wall", 1);
            this.loadTexture("#ground-image", "ground", 1);
            this.loadTexture("#blooded-ground-image", "blooded-ground", 1);
            this.loadTexture("#skull-ground-image", "skull-ground", 1);
        }
    }

    // Load as Singleton
    if(getModules.ResourceManagerInstance == undefined)
    {
        getModules.ResourceManagerInstance = new ResourceManager();
    }

    return exports;
};
