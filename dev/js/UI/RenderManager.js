/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.RenderManagerModule = function () {
    let exports = {};

    class RenderManager
    {
        /**
         * Constructor.
         * @param context Canvas context.
         * @param pixelPerUnit Count of pixel by abstract unit
         */
        constructor(context, pixelPerUnit = 100)
        {
            this._context = context;

            // manage Abstract Coordinates
            let canvas = this._context.canvas;

            this._pixelPerUnit = pixelPerUnit;
            this._ratioWidth = canvas.width / canvas.offsetWidth;
            this._ratioHeight = canvas.height / canvas.offsetHeight;
            this._offsetX = canvas.width / 2;
            this._offsetY = canvas.height / 2;

            window.addEventListener('resize', (function () {
                let canvas = this._context.canvas;
                this._ratioWidth = canvas.width / canvas.offsetWidth;
                this._ratioHeight = canvas.height / canvas.offsetHeight;
            }).bind(this));
        }

        abstractToCanvasX(x)
        {
            return x * this._ratioWidth * this._pixelPerUnit;
        }

        abstractToCanvasY(y)
        {
            return y * this._ratioHeight * this._pixelPerUnit;
        }

        /**
         * Render level.
         * @param level Level to render.
         */
        renderLevel(level)
        {
            // Clear Canvas
            this._context.clearRect(0, 0, this._context.canvas.width, this._context.canvas.height);

            let ResourceManager = getModules.ResourceManagerInstance;
            let wall = ResourceManager.getTexture("wall");
            let ground = ResourceManager.getTexture("ground");
            let bloodedGround = ResourceManager.getTexture("blooded-ground");
            let skullGround = ResourceManager.getTexture("skull-ground");
            // Render Level map
            for (let y = 0, mLength = level._map.length ; y < mLength ; ++y)
            {
                let line = level._map[y];
                for (let x = 0 , lLength = line.length; x < lLength ; ++x)
                {
                    let targetX = x - level._player._x;
                    let targetY = y - level._player._y;
                    switch (line[x])
                    {
                        case '-1':
                            wall.render(this, targetX, targetY);
                            break;
                        case '0':
                        case 'I':
                        case 'O':
                        case 'S':
                            ground.render(this, targetX, targetY);
                            break;
                        case 'G':
                            bloodedGround.render(this, targetX, targetY);
                            break;
                        case 'D':
                            skullGround.render(this, targetX, targetY);
                            break;
                    }
                }
            }

            // Render Hero
            this.renderFixEntity(level._player);

            // Render enemies
            let offset = { offX : -level._player._x, offY : -level._player._y };
            for (let i = 0 ; i < level._enemies.length ; ++i)
            {
                this.renderEntity(level._enemies[i], offset.offX, offset.offY);
            }
        }

        /**
         * Render entity.
         * @param entity Entity to render.
         */
        renderFixEntity(entity)
        {
            entity.renderFix(this);
        }

        /**
         * Render entity.
         * @param entity Entity to render.
         * @param offsetX Offset X to apply to draw.
         * @param offsetY Offset Y to apply to draw.
         */
        renderEntity(entity, offsetX = 0, offsetY = 0)
        {
            entity.render(this, offsetX, offsetY);
        }

        renderImage(image, xClip, yClip, widthClip, heightClip, xDraw, yDraw, widthDraw, heightDraw)
        {
            let canvasX = this.abstractToCanvasX(widthDraw);
            let canvasY = this.abstractToCanvasY(heightDraw);

            // Draw the sprite
            this._context.drawImage(image,
                                    xClip,
                                    yClip,
                                    widthClip,
                                    heightClip,
                                    this.abstractToCanvasX(xDraw) + this._offsetX - canvasX / 2,
                                    this.abstractToCanvasY(yDraw) + this._offsetY - canvasY / 2,
                                    canvasX,
                                    canvasY
            );
        }

    }

    exports.RenderManagerClass = RenderManager;

    return exports;
};