/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.DisplayableModule = function () {
    let exports = {};

    class Displayable
    {
        /**
         * Constructor.
         * @param options Options contains these parameters:
         * context: Canvas 2D context.
         * sizeX: Sprite display width.
         * sizeY: Sprite display height.
         * image: Image.
         */
        constructor(options)
        {
            this._displayWidth = options.sizeX;
            this._displayHeight = options.sizeY;

            this._image = options.image;
        }

        /**
         * Render.
         * @param renderManager The RenderManager to use for rendering.
         * @param x X position where rendering.
         * @param y Y position where rendering.
         */
        render(renderManager, x = 0, y = 0)
        {
        };
    }

    exports.DisplayableClass = Displayable;

    return exports;
};