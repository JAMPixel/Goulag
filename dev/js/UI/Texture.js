/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.TextureModule = function () {
    let exports = {};

    class Texture extends getModules.DisplayableModule().DisplayableClass
    {
        /**
         * Constructor.
         * @param options Options contains these parameters:
         * context: Canvas 2D context.
         * sizeX: Texture display width.
         * sizeY: Texture display height.
         * image: Image.
         */
        constructor(options)
        {
            super(options);
        }

        /**
         * Render texture
         * @param renderManager the RenderManager to use for rendering.
         * @param x X position where rendering sprite.
         * @param y Y position where rendering sprite.
         */
        render(renderManager, x = 0, y = 0)
        {
            super.render(renderManager, x, y);

            // Draw the texture
            renderManager.renderImage(  this._image,
                                        0,
                                        0,
                                        this._image.clientWidth,
                                        this._image.clientHeight,
                                        x,
                                        y,
                                        this._displayWidth,
                                        this._displayHeight);
        };
    }

    exports.TextureClass = Texture;

    return exports;
};