/*!
 * @file DrawManager
 * @author Justin Goutey.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.DrawManagerModule = function () {
    var exports = {};

    class DrawManager
    {
        constructor()
        {
            this._canvas = document.querySelector("#GameCanvas");
        }
    }

    exports.DrawManagerClass = DrawManager;

    return exports;
};