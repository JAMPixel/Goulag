/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.SpriteModule = function () {
    let exports = {};

    class Sprite extends getModules.DisplayableModule().DisplayableClass
    {
        /**
         * Constructor.
         * @param options Options contains these parameters:
         * context: Canvas 2D context.
         * width: Sprite width.
         * height: Sprite height.
         * sizeX: Sprite display width.
         * sizeY: Sprite display height.
         * image: Full sprite image.
         * ticksPerFrame: Number of frames needed before changing sprite image.
         * numberOfFrames: Number of frames contained in the sprite image.
         */
        constructor(options)
        {
            super(options);

            // The current frame to be displayed
            this._frameIndex = 0;
            // The number updates since the current frame was first displayed
            this._tickCount = 0;
            // The number updates until the next frame should be displayed
            this._ticksPerFrame = options.ticksPerFrame || 0;
            // Number of frames in sprite
            this._numberOfFrames = options.numberOfFrames || 1;

            this._loop = typeof options.loop === 'undefined' ? true : options.loop;
        }

        /**
         * Render sprite
         * @param renderManager the RenderManager to use for rendering.
         * @param x X position where rendering sprite.
         * @param y Y position where rendering sprite.
         */
        render(renderManager, x = 0, y = 0)
        {
            super.render(renderManager, x, y);

            renderManager.renderImage(  this._image,
                                        this._frameIndex * this._image.clientWidth / this._numberOfFrames,
                                        0,
                                        this._image.clientWidth / this._numberOfFrames,
                                        this._image.clientHeight,
                                        x,
                                        y,
                                        this._displayWidth,
                                        this._displayHeight);
        }

        /**
         * Update sprite (each update call is considered as a frame being displayed).
         */
        update()
        {
            this._tickCount += 1;

            if (this._tickCount > this._ticksPerFrame)
            {
                this._tickCount = 0;

                // If the current frame index is in range
                if (this._frameIndex < this._numberOfFrames - 1)
                {
                    // Go to the next frame
                    this._frameIndex += 1;
                }
                // Loop on sprite
                else if (this._loop)
                {
                    this._frameIndex = 0;
                }
            }
        };
    }

    exports.SpriteClass = Sprite;

    return exports;
};