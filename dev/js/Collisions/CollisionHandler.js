/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 12 février 2017.
 * @brief
 * @addtogroup
 */

getModules.CollisionHandlerModule = function () {
    let exports = {};

    class BoundingCollision
    {
        static collide(entity1, entity2)
        {
            return  entity1._x + entity1._sprite._displayWidth >= entity2._x
                    && entity1._x <= entity2._x + entity2._sprite._displayWidth
                    && entity1._y + entity1._sprite._displayHeight >= entity2._y
                    && entity1._y <= entity2._y + entity2._sprite._displayHeight;
        }
    }

    exports.BoundingCollisionClass = BoundingCollision;

    return exports;
};