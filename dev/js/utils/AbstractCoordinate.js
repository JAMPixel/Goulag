/*!
 * @file AbstractCoordinate
 * @author Justin Goutey.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.AbstractCoordinateModule = function () {
    let exports = {};

    class AbstractCanvasCoordinate
    {
        constructor(canvasId,pixelUnit,x = 0, y = 0)
        {
            this._canvas = document.querySelector(canvasId);
            this._canvasWidth = this._canvas.width;
            this._canvasHeight = this._canvas.height;
            this._ratioWidth = this._canvasWidth/this._canvas.offsetWidth;/* canvas/px */
            this._ratioHeight = this._canvasHeight/this._canvas.offsetHeight;/* canvas/px */
            this._pixelUnit = pixelUnit;
            this._x = x;
            this._y = y;

            // udpdate
            window.addEventListener('resize',(function () {
                this._ratioWidth = this._canvasWidth/this._canvas.offsetWidth;
                this._ratioHeight = this._canvasHeight/this._canvas.offsetHeight;
            }).bind(this))
        }

        xPix()
        {
            return this._x * this._pixelUnit;
        }

        yPix()
        {
            return this._y * this._pixelUnit;
        }

        xCanvas()
        {
            return this._x * this._pixelUnit * this._ratioWidth;
        }

        yCanvas()
        {
            return this._y * this._pixelUnit * this._ratioHeight;
        }
    }

    exports.AbstractCanvasCoordinateClass = AbstractCanvasCoordinate;
    return exports;
};