/*!
 * @file CenterObject
 * @author Justin Goutey.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.CenterObjectModule = function () {
    let exports = {};

    class CenterObject
    {
        /**
         * @brief
         * @param objectId HTML id of the object to center in the window.
         * @param minWidth Minimal width to set to the object
         * @param rapport Rapport between height and width for th object : width/height
         * @param margin Margin to force around the object
         */
        constructor(objectId, minWidth, rapport, margin)
        {
            this._centeredObject = document.querySelector(objectId);
            this._minWidth = minWidth;
            this._ratio = rapport;
            this._minHeight = minWidth/rapport;
            this._margin = margin;

            this.adaptSize();

            //recenter on window's resizing
            window.addEventListener("resize", (function () {
                console.log("window resize");
                this.adaptSize();
            }).bind(this));
        }

        /**
         * @brief This method resize the centered object to make it
         */
        adaptSize()
        {
            let width = this._minWidth;//this._centeredObject.clientWidth;
            let height = this._minHeight;//this._centeredObject.clientHeight;

            let windowWidth = window.innerWidth-this._margin;
            let windowHeight = window.innerHeight-this._margin;

            // width = (width<windowWidth) ? windowWidth : width;
            // height = (height<windowHeight) ? windowHeight : height;
            // this._centeredObject.style.width = (width<windowWidth)

            if(windowWidth<width || windowHeight<height)
            {
                // not enough center => left = 0
                this._centeredObject.style.width = width+'px';
                this._centeredObject.style.height = height+'px';
                this._centeredObject.style.position = 'absolute';
                this._centeredObject.style.top = this._margin+'px';
                this._centeredObject.style.left = this._margin+'px';
            }
            else
            {
                if(windowWidth/windowHeight > this._ratio)
                {
                    width = windowHeight*this._ratio;
                    height = windowHeight;
                }
                else
                {
                    width = windowWidth;
                    height = windowWidth/this._ratio;
                }

                this._centeredObject.style.width = width+'px';
                this._centeredObject.style.height = height+'px';
                this._centeredObject.style.position = 'fixed';
                this._centeredObject.style.top = (windowHeight/2-height/2)+'px';
                this._centeredObject.style.left = (windowWidth/2-width/2)+'px';
            }
        }
    }

    exports.CenterObjectClass = CenterObject;

    return exports;
};