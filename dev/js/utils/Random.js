/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 12 février 2017.
 * @brief
 * @addtogroup
 */

getModules.RandomModule = function () {
    let exports = {};

    class Random
    {
        static getRandom(min, max)
        {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }

    exports.RandomClass = Random;

    return exports;
};