/*!
 * @file Level
 * @author Justin Goutey.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.LevelModule = function () {
    let exports = {};

    const ResourceManager = getModules.ResourceManagerInstance;
    const Random = getModules.RandomModule().RandomClass;
    const BoundingCollision = getModules.CollisionHandlerModule().BoundingCollisionClass;

    class Level
    {
        constructor(levelFile)
        {
            // Create hero
            const Hero = getModules.HeroModule().HeroClass;
            this._player = new Hero(0, 0, 0.1, ResourceManager.getSprite("hero-standby1"));

            this._enemies = [];

            // Get level
            var xhttp = new XMLHttpRequest();
            let level = this;
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                {
                    // Action to be performed when the document is read;
                    let tab = xhttp.responseText.split('\n');
                    level._map = [];
                    tab.forEach(function (elt) {
                        level._map.push(elt.split(';'));
                    });

                    let initCoord = level.findPoint(level._map,'I');

                    level._endCoord = level.findPoint(level._map,'O');

                    // Set initial position of hero
                    level._player._x = initCoord.x;
                    level._player._y = initCoord.y;

                    // Set positions of enemies
                    const Entity = getModules.EntityModule().EntityClass;
                    let ghostSprite = ResourceManager.getSprite("ghost");
                    let spawns = level.findAllPoints(level._map, 'S');
                    for (let i = 0 ; i < spawns.length ; ++i)
                    {
                        level._enemies.push(new Entity(spawns[i].x, spawns[i].y, 0.1, ghostSprite));
                    }
                }
            };
            xhttp.open("GET", levelFile, true);
            xhttp.send();

            window.addEventListener('keydown', (this.movePlayer).bind(this));
        }

        /**
         * Callback when player press keys.
         * @param event Keyboard event.
         */
        movePlayer(event)
        {
            if (!this._player._isAlive)
                return;

            let dirX = 0;
            let dirY = 0;

            switch (event.code)
            {
                case 'ArrowUp':
                case 'KeyW':
                    this._player.setSprite(ResourceManager.getSprite("hero-top"));
                    dirY = -1;
                    break;
                case 'ArrowLeft':
                case 'KeyA':
                    this._player.setSprite(ResourceManager.getSprite("hero-left"));
                    dirX = -1;
                    break;
                case 'ArrowDown':
                case 'KeyS':
                    this._player.setSprite(ResourceManager.getSprite("hero-bottom"));
                    dirY = 1;
                    break;
                case 'ArrowRight':
                case 'KeyD':
                    this._player.setSprite(ResourceManager.getSprite("hero-right"));
                    dirX = 1;
                    break;
            }

            this.moveEntity(this._player, dirX, dirY);
        }

        /**
         * Move all enemies entities.
         */
        moveEnemies()
        {
            for (let i = 0 ; i < this._enemies.length ; ++i)
            {
                let dirX = Random.getRandom(-1, 1);
                let dirY = Random.getRandom(-1, 1);

                // Fix direction chosen => Deletable
                if (dirX != 0 && dirY != 0)
                {
                    if (Random.getRandom(0, 1))
                    {
                        dirX = 0;
                    }
                    else
                    {
                        dirY = 0;
                    }
                }

                this.moveEntity(this._enemies[i], dirX, dirY);

                if (BoundingCollision.collide(this._player, this._enemies[i]))
                {
                    this._player.die();
                }
            }
        }

        /**
         * Move entity if it does not go into a wall.
         * @param entity Entity to move.
         * @param dirX Direction on X.
         * @param dirY Direction on Y.
         */
        moveEntity(entity, dirX, dirY)
        {
            //let targetX = dirX > 0 ? Math.ceil(entity._x + dirX) : dirX == 0 ? Math.round(entity._x + dirX) : Math.floor(entity._x + dirX);
            let targetX = dirX > 0 ? Math.floor(entity._x) + dirX : dirX == 0 ? Math.round(entity._x) + dirX : Math.ceil(entity._x) + dirX;
            //let targetY = dirY > 0 ? Math.ceil(entity._y + dirY) : dirY == 0 ? Math.round(entity._y + dirY) : Math.floor(entity._y + dirY);
            let targetY = dirY > 0 ? Math.floor(entity._y) + dirY : dirY == 0 ? Math.round(entity._y) + dirY : Math.ceil(entity._y) + dirY;

            if (this.checkMove(targetX, targetY))
            {
                entity.move(dirX, dirY);

                // Handle specific cases
                let target = this._map[targetY][targetX];
                if (target == 'D')
                {
                    setTimeout(function (player, target) {
                        if (Math.round(player._x) == target.targetX && Math.round(player._y) == target.targetY)
                        {
                            player.die();
                        }
                    }, 1700, this._player, {targetX: targetX, targetY: targetY});
                }
            }
        }

        /**
         * Check if the move to target is possible.
         * @param targetX Target position X.
         * @param targetY Target position Y.
         * @return boolean True if move is possible.
         */
        checkMove(targetX, targetY)
        {
            let ret = false;

            let height = this._map.length;
            let width = this._map[0].length;   // Map should contains at least one line
            if (targetX >= 0 && targetX < width
                && targetY >= 0 && targetY < height)
            {
                let targetValue = this._map[targetY][targetX];
                // Every positive character is walkable
                if (targetValue >= '0')
                {
                    ret = true;
                }
            }

            return ret;
        }

        findPoint(map, char)
        {
            let coord = {x:0,y:0};

            coord.y = map.findIndex(function (eltn1)
            {
                let res = eltn1.findIndex(function (eltn2)
                {
                    return eltn2 == char;
                });

                if(res != -1)
                {
                    coord.x = res;
                    return true;
                }
            });

            return coord;
        }

        endLevel()
        {
            return (this._player!=undefined && this._endCoord != undefined) ? this._player._x+0.5>this._endCoord.x && this._endCoord.x+1>this._player._x+0.5 && this._player._y+0.5>this._endCoord.y && this._endCoord.y+1>this._player._y+0.5 : false;
        }

        findAllPoints(map, char)
        {
            let coords = [];

            for (let y = 0 ; y < map.length ; ++y)
            {
                let line = map[y];
                for (let x = 0 ; x < line.length ; ++x)
                {
                    if (line[x] == char)
                    {
                        coords.push({ x: x, y: y });
                    }
                }
            }

            return coords;
        }
    }

    exports.LevelClass = Level;

    return exports;
};