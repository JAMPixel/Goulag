/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

getModules.GameModule = function () {
    let exports = {};

    class Game
    {
        /**
         * Constructor.
         * @param context Canvas context.
         */
        constructor(context)
        {
            const Level = getModules.LevelModule().LevelClass;
            const Renderer = getModules.RenderManagerModule().RenderManagerClass;

            this._currentLevelId = 0;
            // Construct Level
            this._level = new Level(levelList[this._currentLevelId]);

            // Renderer
            this._renderer = new Renderer(context);
            this._endShadow = new (getModules.CenterObjectModule().CenterObjectClass)("#end_shadow", 600, 16/9, 5);
        }

        /**
         * Game loop.
         */
        gameLoop()
        {
            // Enf level Player dead and animation finished
            if (this._level.endLevel())
            {
                window.requestAnimationFrame(this.gameEnd.bind(this));
            }
            else if (!this._level._player._isAlive && (this._level._player._sprite._numberOfFrames - 1)  == this._level._player._sprite._frameIndex)
            {
                window.requestAnimationFrame(this.gameLoose().bind(this));
            }
            else
            {
                window.requestAnimationFrame(this.gameLoop.bind(this));
            }

            // Handle moves
            this._level.moveEnemies();

            // Render all level
            this._renderer.renderLevel(this._level);
        }

        /**
         * Game End
         */
        gameEnd()
        {
            menu._musicPlayer.pause();
            this._endShadow._centeredObject.style.visibility = 'visible';
            this._endShadow._centeredObject.innerHTML = this._endShadow._centeredObject.innerHTML+"<iframe id='end_music_iframe' width='0' height='0' src='https://www.youtube.com/embed/5lL5EXhFCxU?autoplay=1'></iframe>";


            document.querySelector('#previous_game_button').addEventListener('click',(function () {
                let endMusicIframe = document.querySelector("#end_music_iframe");
                endMusicIframe.parentNode.removeChild(endMusicIframe);
                menu._musicPlayer.play();
                const Level = getModules.LevelModule().LevelClass;
                this._currentLevelId = (this._currentLevelId-1<0) ? levelList.length-1 : this._currentLevelId-1;
                this._level = new Level(levelList[this._currentLevelId]);
                this._endShadow._centeredObject.style.visibility = 'hidden';
                this.gameLoop();
            }).bind(this));

            document.querySelector('#next_game_button').addEventListener('click',(function () {
                let endMusicIframe = document.querySelector("#end_music_iframe");
                menu._musicPlayer.play();
                endMusicIframe.parentNode.removeChild(endMusicIframe);
                this._endShadow._centeredObject.style.visibility = 'hidden';
                const Level = getModules.LevelModule().LevelClass;
                this._currentLevelId = (this._currentLevelId+1>=levelList.length) ? 0 : this._currentLevelId+1;
                this._level = new Level(levelList[this._currentLevelId]);

                this.gameLoop();
            }).bind(this));
        }

        /**
         * Game Loose
         */
        gameLoose()
        {
            menu._musicPlayer.pause();
            // this._endShadow._centeredObject.style.visibility = 'visible';
            let endDiv = document.createElement('div');
            endDiv.style.display = 'flex';
            endDiv.style.justifyContent = 'space-around';
            endDiv.innerHTML = "<iframe id='end_music_iframe' width='600' height='400' style='position: fixed;top: 0;' src='https://www.youtube.com/embed/Ip7QZPw04Ks?autoplay=1'></iframe>";

            document.querySelector('body').appendChild(endDiv);

            setTimeout((function () {
                this._toRemove.parentNode.removeChild(this._toRemove);
                menu._musicPlayer.play();
                this._game._level = new (getModules.LevelModule().LevelClass)(levelList[this._game._currentLevelId]);
                this._game.gameLoop();
            }).bind({_game:this, _toRemove:endDiv}),10000);
        }
    }

    exports.GameClass = Game;

    return exports;
};