/*!
 * @file goulag-game.js
 * @author Alexandre Rabérin.
 * @date 11 février 2017.
 * @brief
 * @addtogroup
 */

const CenterObject = getModules.CenterObjectModule().CenterObjectClass;
const Game = getModules.GameModule().GameClass;
const SlideMenu = getModules.MenuModule().SlideMenuClass;
getModules.ResourceManagerModule();

// Get canvas
const canvasCenter = new CenterObject("#GameCanvas", 600, 16/9, 10);

// Load all resources
getModules.ResourceManagerInstance.loadResources();

// Create game object
let game = new Game(canvasCenter._centeredObject.getContext("2d"));

// Init menu
const menu = new SlideMenu("#menu");

// Start the game loop as soon as all things are loaded
window.addEventListener("load", (game.gameLoop).bind(game));