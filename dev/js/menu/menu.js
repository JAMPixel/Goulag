/**
 * Created by ju on 19/01/17.
 */

getModules.MenuModule = function () {
    let exports = {};


    class SlideMenu
    {
        constructor(menuId, menuButtonId = undefined)
        {
            this._menu          = document.querySelector(menuId);
            if(menuButtonId != undefined)
            {
                this._menuButton    = document.querySelector(menuButtonId);
            }

            // Setup Music Player
            this._musicPlayer = new (getModules.PlayerModule().PlayerClass)();
            this._dragableMusicPlayer = new (getModules.DragAndDropModule().DraggableClass)(document.querySelector('#player_bubble'),document.querySelector('#player'),25,25);

            // Html elements styles
            this._menu.style.height = window.innerHeight+"px";
            this._menu.style.left = -this._menu.offsetWidth+"px";

            // setup level list
            this._levelList = document.querySelector("#level_list");
            levelList.forEach((function (elt) {
                this._levelList.appendChild(SlideMenu.generateLevelElement(elt));
            }).bind(this));

            // Events
                // make the menu fit in the windows height
            window.addEventListener("resize", (function (e) {
                this.style.height = window.innerHeight+"px";
            }).bind(this._menu));

                // make the menu display or hidden when clicking on menu button
            if(menuButtonId == undefined)
            {
                window.addEventListener('keydown',(function (event) {
                    if(event.keyCode == 32)
                    {
                        let width = this.offsetWidth;
                        if(this.style.left == "0px") // the menu is displayed
                        {
                            this.style.left = -width+"px";
                        }
                        else
                        {
                            this.style.left = 0;
                        }
                    }
                }).bind(this._menu));
            }
            else
            {
                this._menuButton.addEventListener("click", (function (e) {
                    let width = this.offsetWidth;
                    if(this.style.left == "0px") // the menu is displayed
                    {
                        this.style.left = -width+"px";
                    }
                    else
                    {
                        this.style.left = 0;
                    }

                }).bind(this._menu));
            }
        }

        static generateLevelElement(filename)
        {
            let element = document.createElement('div');
            element.className = "button";
            element.addEventListener('click',(function (event) {
                console.log("Troll");

                let Level = getModules.LevelModule().LevelClass;
                delete (this._game)._level;
                this._game._level = new Level(this._filename);
                this._game._endShadow._centeredObject.style.visibility = 'hidden';
                this._game.gameLoop();
            }).bind({_game:game,_filename:filename}));

            element.textContent = filename.split('/').reverse()[0].split('.')[0];

            return element;
        }
    }

    exports.SlideMenuClass = SlideMenu;
    return exports;
};