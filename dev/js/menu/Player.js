/**
 * Created by ju on 16/12/16.
 */



getModules.PlayerModule = function () {
    let exports = {};
    let ScrollingTextClass = getModules.ScrollingTextModule().ScrollingTextClass;
    class Player
    {
        // Constructor
        constructor()
        {
            this._currentSongId             = Math.floor(Math.random()*songList.length);
            this.totalTime_                 = 0;
            this.totalTimeFormat_           = '00:00:00';
            this.state_                     = false;
            this.timeBar_                   = document.querySelector('#time_bar_current');
            this.timeText_                  = document.querySelector('#time_text');
            this.titleText_                 = new ScrollingTextClass(document.querySelector('#title_text'),20);
            this.buttonPrevious_            = document.querySelector('#previous_button');
            this.buttonPlayPause_           = document.querySelector('#play_pause_button');
            this.buttonNext_                = document.querySelector('#next_button');
            this.buttonIncrease_            = document.querySelector('#increase_volume_button');
            this.buttonDecrease_            = document.querySelector('#decrease_volume_button');
            this.playerCommandsContour_     = {domElt: document.querySelector('#player_commands_contour'),state:true};
            this.bubble_                    = document.querySelector('#player_bubble');
            this.playerCommandsContourSize_ = this.playerCommandsContour_.domElt.style.height;
            this.hiddenable_                = true;
            this._audioElement              = document.querySelector("#audio_player_balise");

            this._audioElement.src = songList[this._currentSongId];
            this._audioElement.play();
            (this._audioElement.paused) ? this.buttonPlayPause_.style.backgroundImage = 'url("/images/player/play.png")' : this.buttonPlayPause_.style.backgroundImage = 'url("/images/player/break.png")';

            // Player Events
            this.buttonPrevious_.onclick = (function () {
                console.log("previous");
                this._currentSongId = (this._currentSongId-1<0) ? songList.length-1 : this._currentSongId-1;
                this._audioElement.src = songList[this._currentSongId];
                // this.updateMetaData();
                this._audioElement.play();
            }).bind(this);

            this.buttonPlayPause_.onclick = (function (e) {
                let imageStyle = e.target.style;
                if(imageStyle.backgroundImage == 'url("/images/player/break.png")')
                {
                    imageStyle.backgroundImage = "url('/images/player/play.png')";
                    this.pause();
                }
                else
                {
                    imageStyle.backgroundImage = "url('/images/player/break.png')";
                    this.play();
                }
            }).bind(this._audioElement);

            this.buttonNext_.onclick = (function () {
                console.log('next');
                this._currentSongId = (this._currentSongI+1>=songList.length) ? 0 : this._currentSongId+1;
                this._audioElement.src = songList[this._currentSongId];
                // this.updateMetaData();
                this._audioElement.play();
            }).bind(this);

            this.buttonIncrease_.onclick = (function () {
                this.volume = (this.volume>0.9) ? 1 : this.volume+0.1;
            }).bind(this._audioElement);

            this.buttonDecrease_.onclick = (function () {
                this.volume = (this.volume<0.1) ? 0 : this.volume-0.1;
            }).bind(this._audioElement);
            //
            document.addEventListener('mousemove',(function (e) {
                this.onMove = true;
            }).bind(this));

            this.bubble_.addEventListener('mousedown',(function (e) {
                if(e.button == 0)
                {
                    this.hiddenable_ = true;
                    setTimeout((function () {
                        this.hiddenable_ = false;
                    }).bind(this),150);
                }
            }).bind(this));

            this.bubble_.addEventListener('mouseup',(function (e) {
                if(e.button == 0 && this.hiddenable_)
                {
                    if(this.playerCommandsContour_.state)
                    {
                        this.playerCommandsContour_.domElt.style.height = '0px';
                        this.playerCommandsContour_.domElt.style.width = '0px';
                        this.playerCommandsContour_.state = false;
                    }
                    else
                    {
                        this.playerCommandsContour_.domElt.style.height = this.playerCommandsContourSize_;
                        this.playerCommandsContour_.domElt.style.width = '100%';
                        this.playerCommandsContour_.state = true;
                    }
                }
            }).bind(this));

            this._audioElement.addEventListener('canplay',(function (event) {

                this.updateMetaData();
            }).bind(this));

            this._audioElement.addEventListener('ended', (function (event) {
                console.log('next');
                this._currentSongId = (this._currentSongI+1>=songList.length) ? 0 : this._currentSongId+1;
                this._audioElement.src = songList[this._currentSongId];
                this._audioElement.play();
            }).bind(this));

            this._audioElement.addEventListener('timeupdate', (function (event) {
                let time = this._audioElement.currentTime;
                let sec = Math.floor(time)%60;
                let min = Math.floor(time%3600/60);
                let hour = Math.floor(time/3600);
                this.timeText_.textContent = ((hour<10) ? '0'+hour:hour)+':'+((min<10) ? '0'+min:min)+':'+((sec<10) ? '0'+sec:sec)+'/'+this.totalTimeFormat_;
                this.timeBar_.style.width = (100*time/this._audioElement.duration)+'%';
            }).bind(this));
        };
        // Methods
        pause()
        {

            this.buttonPlayPause_.backgroundImage = "url('/images/player/play.png')";
            this._audioElement.pause();
        }

        play()
        {

            this.buttonPlayPause_.backgroundImage = "url('/images/player/break.png')";
            this._audioElement.play();
        }

        updateMetaData()
        {
            this.titleText_.setText(this._audioElement.src.split('/').reverse()[0].split('.')[0]);
            let time = this._audioElement.duration;
            let sec = Math.floor(time)%60;
            let min = Math.floor(time%3600/60);
            let hour = Math.floor(time/3600);
            this.totalTimeFormat_ = ((hour<10) ? '0'+hour:hour)+':'+((min<10) ? '0'+min:min)+':'+((sec<10) ? '0'+sec:sec);
        }
    }

    exports.PlayerClass = Player;
    return exports;
};