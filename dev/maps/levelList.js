/*!
 * @file levelList.js
 * @author Justin Goutey.
 * @date 12 février 2017.
 * @brief
 * @addtogroup
 */

var levelList = [
    'maps/level1.csv',
    'maps/level2.csv',
    'maps/level3.csv',
    'maps/level4.csv',
	'maps/level5.csv',
    'maps/level6.csv'
];