/*!
 * @file songList
 * @author Justin Goutey.
 * @date 12 février 2017.
 * @brief
 * @addtogroup
 */

var songList = [
    "song/L_International_Communiste_Version_(Longue)_Française.mp3",
    "song/Red_Army_Choir__Ah_Nastassia..mp3",
    "song/Red_Army_Choir__Along_Peterskaia_Street..mp3",
    "song/Red_Army_Choir__Bella_Ciao..mp3",
    "song/Red_Army_Choir__Civil_War_Songs..mp3",
    "song/Red_Army_Choir__Cossack_s_Song..mp3",
    "song/Red_Army_Choir__Echelon_s_Song..mp3",
    "song/Red_Army_Choir__Gandzia..mp3",
    "song/Red_Army_Choir__In_the_Central_Steppes..mp3",
    "song/Red_Army_Choir__Korobeiniki..mp3",
    "song/Red_Army_Choir__Let_s_Go..mp3",
    "song/Red_Army_Choir__Moscow_Nights..mp3",
    "song/Red_Army_Choir__My_Army..mp3",
    "song/Red_Army_Choir__My_Country..mp3",
    "song/Red_Army_Choir__Oh_Fields,_My_fields..mp3",
    "song/Red_Army_Choir__On_the_Road..mp3",
    "song/Red_Army_Choir__Partisan_s_Song..mp3",
    "song/Red_Army_Choir__Smuglianka..mp3",
    "song/Red_Army_Choir__Song_of_the_Volga_Boatman..mp3",
    "song/Red_Army_Choir__Souliko..mp3",
    "song/Red_Army_Choir__The_Cliff..mp3",
    "song/Red_Army_Choir__The_Cossacks..mp3",
    "song/Red_Army_Choir__The_Red_Army_Is_the_Strongest..mp3",
    "song/Red_Army_Choir__The_Roads..mp3",
    "song/Red_Army_Choir__Troika_Galop..mp3"
];
