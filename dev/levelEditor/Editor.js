/*!
 * @file Editor
 * @author Justin Goutey.
 * @date 12 février 2017.
 * @brief
 * @addtogroup
 */

class Editor
{
    constructor(width,height)
    {
        // clickable area
        this._container = document.querySelector("#editable_area");

        let div = document.createElement('div');
        let line = div.cloneNode(true);
        div.className = "unit";
        line.className = "unit_line";
        this._map = [];
        for(let l = 0;l<height;++l)
        {
            let newTab = [];

            let newLine = line.cloneNode(true);
            newLine.dataLid = l;

            for(let c = 0;c<width;++c)
            {
                let newDiv = div.cloneNode(true);
                newDiv.dataCid = c;

                newDiv.addEventListener('click',(function (event) {
                    let target = event.target;
                    let c = target.dataCid;
                    let l = target.parentNode.dataLid;

                    if(this._map[l][c] == 0)
                    {
                        target.style.backgroundColor = "#448E23";
                        this._map[l][c] = -1;
                    }
                    else
                    {
                        target.style.backgroundColor = "rgba(0,0,0,0)";
                        this._map[l][c] = 0;
                    }
                }).bind(this));

                newLine.appendChild(newDiv);
                newTab.push(0);
            }
            this._container.appendChild(newLine);
            this._map.push(newTab);
        }

        // Log Button:
        document.querySelector('#log_button').addEventListener('click',(function (event) {
            this.logMap();
        }).bind(this));
    }

    logMap()
    {
        let map = this._map;

        // map.forEach(function (line) {
        //     line.forEach(function (column) {
        //
        //     })
        // });

        let string = map.reduce(function (resPrecN1, currentValueN1) {
            return resPrecN1+'\n'+currentValueN1.reduce(function (resPrecN2, currentValueN2) {
                    return resPrecN2+currentValueN2+';';
                },'');
        }, '');

        console.log(string);
    }

}

let editor = new Editor(60,60);